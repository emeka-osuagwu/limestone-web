import React, { Component } from 'react';

import MapView from '../Map/MapView';
import axios from '../config/axios';
import { geolocated } from 'react-geolocated';

import ReactLoading from 'react-loading';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            locations: [],
            mapCenter: {},
            loading: true,
        };
    }

    async componentDidMount() {
        axios
            .get('/property?at=52.510398,13.38857')
            .then((response) => {
                this.setState({
                    locations: response.data.data.results.items,
                    mapCenter: {
                        lat: response.data.data.results.items[0].position[0],
                        lng: response.data.data.results.items[0].position[1],
                    },
                    loading: false,
                });
            })
            .catch((error) => {
                console.log(error);
            });
    }
    emeka() {
        alert('dfjkdjfj');
    }
    async handleBookHodel(location) {
        const { title, distance, id, icon, position } = location;

        const request_data = {
            icon,
            distance,
            position: JSON.stringify(position),
            name: title,
            property_id: id,
            image: 'https://picsum.photos/200/300',
        };

        this.setState({ loading: true });

        await axios.post('/booking', request_data).then((response) => {
            alert('Hotel Booked');
            this.setState({ loading: false });
        });
    }

    renderMap() {
        return (
            <div>
                <h3 style={{ padding: 3 }}>
                    <img src="https://res.cloudinary.com/arm/image/upload/v1602457628/design_Limehome_Logo_mslhiq_gufwtr.png" />
                </h3>

                <MapView
                    locations={this.state.locations}
                    mapCenter={this.state.mapCenter}
                    bookHotel={(location) => this.handleBookHodel(location)}
                />
            </div>
        );
    }

    renderLoading() {
        return (
            <div className="loading-con" style={{ paddingTop: 300 }}>
                <center>
                    <ReactLoading
                        type={'spin'}
                        color={'#357FDD'}
                        height={'5%'}
                        width={'5%'}
                    />
                </center>
            </div>
        );
    }

    render() {
        const { loading } = this.state;
        return loading ? this.renderLoading() : this.renderMap();
    }
}

export default geolocated({
    positionOptions: {
        enableHighAccuracy: false,
    },
    userDecisionTimeout: 5000,
})(App);
