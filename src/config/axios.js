import axios from 'axios';

const BACKEND_API = process.env.REACT_APP_BACKEND_API;

export default axios.create({
    baseURL: BACKEND_API,
});
