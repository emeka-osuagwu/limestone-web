import React, { useState } from 'react';

import {
    GoogleMap,
    Marker,
    useLoadScript,
    InfoWindow,
} from '@react-google-maps/api';

import ReactHtmlParser, {
    processNodes,
    convertNodeToElement,
    htmlparser2,
} from 'react-html-parser';

const KEY = process.env.REACT_APP_GOOGLE_MAP_KEY;

const Map = (props) => {
    const { locations, mapCenter, bookHotel } = props;

    const [myMap, setMyMap] = useState(null);
    const [selected, setSelected] = React.useState();

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: KEY,
    });

    const onLoad = (infoWindow) => {
        console.log('infoWindow: ', infoWindow);
    };

    const renderInfoDev = () => {
        const { title, vicinity, openingHours, icon, position } = selected;

        return (
            <div className="info_wrapper">
                <img src="http://lorempixel.com/city/250/200/" />

                <h3>{title}</h3>
                <p>{ReactHtmlParser(vicinity)}</p>

                {openingHours && <p>{openingHours.text}</p>}

                <button
                    className="info_wrapper_button"
                    onClick={() => bookHotel(selected)}>
                    Book
                </button>
            </div>
        );
    };

    const renderMap = () => (
        <GoogleMap
            mapContainerStyle={{
                height: '100vh',
                width: '100%',
            }}
            zoom={14}
            center={mapCenter}
            onLoad={(map) => setMyMap(map)}>
            {locations.map((marker, index) => (
                <>
                    <Marker
                        key={index}
                        position={{
                            lat: marker.position[0],
                            lng: marker.position[1],
                        }}
                        icon={{
                            url: marker.icon,
                            origin: new window.google.maps.Point(0, 0),
                            anchor: new window.google.maps.Point(15, 15),
                            scaledSize: new window.google.maps.Size(40, 40),
                        }}
                        onClick={() => {
                            setSelected(marker);
                        }}
                    />
                </>
            ))}

            {selected ? (
                <InfoWindow
                    onCloseClick={() => {
                        setSelected(null);
                    }}
                    onLoad={onLoad}
                    position={{
                        lat: selected.position[0],
                        lng: selected.position[1],
                    }}>
                    {renderInfoDev()}
                </InfoWindow>
            ) : null}
        </GoogleMap>
    );

    return isLoaded ? renderMap() : null;
};

Map.defaultProps = {
    locations: [],
    mapCenter: {
        lat: 52.521621,
        lng: 13.38857,
    },
    bookHotel: () => {},
};

export default Map;
